# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
hotels = Hotel.create([{name: 'Four Seasons', description: "pretty nice"},
         {name: 'Double Tree', description:'Way better than just one tree'}])
room_types = RoomType.create([
                     { name: 'luxury', price: '200' },
                     { name: 'jacuzzi room', price: '300' },
                     { name: 'dive', price: '50' }])
rooms = Room.create([{ number: 312, hotel: hotels.first, room_type: room_types.first },
                     { number: 28, hotel: hotels.last, room_type: room_types.last },
                     { number: 2, hotel: hotels.last, room_type: room_types.first }])
standard_user = User.create!(email: 'standard2@example.com', password:'password') if Rails.env.development?
bookings = Booking.create([{ check_in: Date.today, check_out: Date.tomorrow, room: rooms.first, user: standard_user  },
                           { check_in: Date.today, check_out: Date.tomorrow, room: rooms.last   }])
AdminUser.create!(email: 'admin2@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
