class AddRoomsCountToHotel < ActiveRecord::Migration[5.1]
  def change
    add_column :hotels, :room_count, :integer, default: 0
  end
end
