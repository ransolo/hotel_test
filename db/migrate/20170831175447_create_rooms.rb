class CreateRooms < ActiveRecord::Migration[5.1]
  def change
    create_table :rooms do |t|
      t.integer :number
      t.string :category
      t.string :description
      t.references :hotel, foreign_key: true
      t.belongs_to :room_type, index: true, foreign_key: true
      t.timestamps
    end
  end
end
