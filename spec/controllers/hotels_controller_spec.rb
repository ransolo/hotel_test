require 'rails_helper'

RSpec.describe HotelsController, type: :controller do
  fixtures :hotels
  # This should return the minimal set of attributes required to create a valid
  # Hotel. As you add validations to Hotel, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    @hotel = hotels(:one)
    { name: @hotel.name, description: @hotel.description }
  }

  let(:invalid_attributes) {
    { name: '', description: 'description' }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # HotelsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      hotel = Hotel.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      hotel = Hotel.create! valid_attributes
      get :show, params: {id: hotel.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      hotel = Hotel.create! valid_attributes
      get :edit, params: {id: hotel.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Hotel" do
        expect {
          post :create, params: {hotel: valid_attributes}, session: valid_session
        }.to change(Hotel, :count).by(1)
      end

      it "redirects to the created hotel" do
        post :create, params: {hotel: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Hotel.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {hotel: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: 'marriot'}
      }

      it "updates the requested hotel" do
        hotel = Hotel.create! valid_attributes
        put :update, params: {id: hotel.to_param, hotel: new_attributes}, session: valid_session
        hotel.reload
        expect(hotel.name).to eql new_attributes[:name]
      end

      it "redirects to the hotel" do
        hotel = Hotel.create! valid_attributes
        put :update, params: {id: hotel.to_param, hotel: valid_attributes}, session: valid_session
        expect(response).to redirect_to(hotel)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        hotel = Hotel.create! valid_attributes
        put :update, params: {id: hotel.to_param, hotel: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested hotel" do
      hotel = Hotel.create! valid_attributes
      expect {
        delete :destroy, params: {id: hotel.to_param}, session: valid_session
      }.to change(Hotel, :count).by(-1)
    end

    it "redirects to the hotels list" do
      hotel = Hotel.create! valid_attributes
      delete :destroy, params: {id: hotel.to_param}, session: valid_session
      expect(response).to redirect_to(hotels_url)
    end
  end

end
