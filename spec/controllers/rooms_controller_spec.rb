require 'rails_helper'

RSpec.describe RoomsController, type: :controller do
  fixtures :rooms
  fixtures :hotels
  # This should return the minimal set of attributes required to create a valid
  # Room. As you add validations to Room, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    @hotel = hotels(:one)
    @room = rooms(:twenty)
    { number: @room.number, description: @room.description, room_type: @room.room_type, hotel_id: @hotel.id }
  }

  let(:invalid_attributes) {
    { number: ''}
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # RoomsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #show" do
    it "returns a success response" do
      room = Room.create! valid_attributes
      get :show, params: {hotel_id: @hotel.id, id: room.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      hotel = hotels(:one)
      get :new, params: {hotel_id: hotel.id}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      room = Room.create! valid_attributes
      get :edit, params: {hotel_id: @hotel.id, id: room.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Room" do
        @hotel = hotels(:one)
        expect {
          post :create, params: {room: valid_attributes, hotel_id: @hotel.id}, session: valid_session
        }.to change(Room, :count).by(1)
      end

      it "redirects to the created room" do
        @hotel = hotels(:one)
        post :create, params: {hotel_id: @hotel.id, room: valid_attributes}, session: valid_session
        expect(response).to redirect_to(hotel_room_path(@hotel, Room.last.id))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        @hotel = hotels(:one)
        post :create, params: {hotel_id: @hotel, room: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        { description: 'Not as nice as it used to be.'}
      }

      it "updates the requested room" do
        @hotel = hotels(:one)
        room = Room.create! valid_attributes
        put :update, params: {hotel_id: @hotel, id: room.to_param, room: new_attributes}, session: valid_session
        room.reload
        expect(room.description).to eql new_attributes[:description]
      end

      it "redirects to the room" do
        @hotel = hotels(:one)
        room = Room.create! valid_attributes
        put :update, params: {hotel_id: @hotel.id, id: room.to_param, room: valid_attributes}, session: valid_session
        expect(response).to redirect_to(hotel_room_path(@hotel, Room.last.id))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        @hotel = hotels(:one)
        room = Room.create! valid_attributes
        put :update, params: {hotel_id: @hotel.id, id: room.to_param, room: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested room" do
      @hotel = hotels(:one)
      room = Room.create! valid_attributes
      expect {
        delete :destroy, params: {hotel_id: @hotel.id, id: room.to_param}, session: valid_session
      }.to change(Room, :count).by(-1)
    end

    it "redirects to the rooms list" do
      @hotel = hotels(:one)
      room = Room.create! valid_attributes
      delete :destroy, params: {hotel_id: @hotel.id, id: room.to_param}, session: valid_session
      expect(response).to redirect_to(hotel_url(@hotel))
    end
  end

end
