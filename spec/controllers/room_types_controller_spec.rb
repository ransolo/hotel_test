require 'rails_helper'

RSpec.describe RoomTypesController, type: :controller do
  fixtures :room_types
  # This should return the minimal set of attributes required to create a valid
  # RoomType. As you add validations to RoomType, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    @room_type = room_types(:one)
    {name: @room_type.name}
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # RoomTypesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      room_type = RoomType.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      room_type = RoomType.create! valid_attributes
      get :show, params: {id: room_type.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      room_type = RoomType.create! valid_attributes
      get :edit, params: {id: room_type.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new RoomType" do
        expect {
          post :create, params: {room_type: valid_attributes}, session: valid_session
        }.to change(RoomType, :count).by(1)
      end

      it "redirects to the created room_type" do
        post :create, params: {room_type: valid_attributes}, session: valid_session
        expect(response).to redirect_to(RoomType.last)
      end
    end

  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {name: room_types(:two).name}
      }

      it "updates the requested room_type" do
        room_type = RoomType.create! valid_attributes
        put :update, params: {id: room_type.to_param, room_type: new_attributes}, session: valid_session
        room_type.reload
        expect(room_type.name).to eql new_attributes[:name]
      end

      it "redirects to the room_type" do
        room_type = RoomType.create! valid_attributes
        put :update, params: {id: room_type.to_param, room_type: valid_attributes}, session: valid_session
        expect(response).to redirect_to(room_type)
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested room_type" do
      room_type = RoomType.create! valid_attributes
      expect {
        delete :destroy, params: {id: room_type.to_param}, session: valid_session
      }.to change(RoomType, :count).by(-1)
    end

    it "redirects to the room_types list" do
      room_type = RoomType.create! valid_attributes
      delete :destroy, params: {id: room_type.to_param}, session: valid_session
      expect(response).to redirect_to(room_types_url)
    end
  end

end
