require 'rails_helper'

RSpec.describe BookingsController, type: :controller do
  # This should return the minimal set of attributes required to create a valid
  # Booking. As you add validations to Booking, be sure to
  # adjust the attributes here as well.
  fixtures :bookings

  let(:valid_attributes) {
    @booking = bookings(:one)
    { check_in: @booking.check_in, check_out: @booking.check_out }
  }

  let(:invalid_attributes) {
    { check_in: 'word', check_out:'up' }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # BookingsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      booking = Booking.create! valid_attributes
      get :show, params: {id: booking.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      booking = Booking.create! valid_attributes
      get :edit, params: {id: booking.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Booking" do
        expect {
          post :create, params: {booking: valid_attributes}, session: valid_session
        }.to change(Booking, :count).by(1)
      end

      it "redirects to the created booking" do
        post :create, params: {booking: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Booking.last)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {booking: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        { check_in: bookings(:two).check_in, check_out: bookings(:two).check_out }
      }
      it "updates the requested booking" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: new_attributes}, session: valid_session
        booking.reload
        expect(booking.check_in).to eql new_attributes[:check_in]
      end

      it "redirects to the booking" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: new_attributes}, session: valid_session
        expect(response).to redirect_to(booking)
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        booking = Booking.create! valid_attributes
        put :update, params: {id: booking.to_param, booking: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested booking" do
      booking = Booking.create! valid_attributes
      expect {
        delete :destroy, params: {id: booking.to_param}, session: valid_session
      }.to change(Booking, :count).by(-1)
    end

    it "redirects to the bookings list" do
      booking = Booking.create! valid_attributes
      delete :destroy, params: {id: booking.to_param}, session: valid_session
      expect(response).to redirect_to(bookings_url)
    end
  end

end
