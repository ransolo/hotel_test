require 'rails_helper'

RSpec.describe "hotels/index", type: :view do
  before(:each) do
    assign(:hotels, [
      Hotel.create!(
        :name => "Name",
        :location => "Location",
        :description => "Description"
      ),
      Hotel.create!(
        :name => "Name",
        :location => "Location",
        :description => "Description"
      )
    ])
  end

  it "renders a list of hotels" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
