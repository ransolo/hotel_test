require 'rails_helper'

RSpec.describe "bookings/edit", type: :view do
  fixtures :bookings
  before(:each) do
    @booking = bookings(:one)
  end

  it "renders the edit booking form" do
    render

    assert_select "form[action=?][method=?]", booking_path(@booking), "post" do

      assert_select "input[name=?]", "booking[user_id]"

    end
  end
end
