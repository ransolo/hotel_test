require 'rails_helper'

RSpec.describe "bookings/index", type: :view do
  fixtures :bookings
  before(:each) do
    assign(:bookings, [
      Booking.create!(
        :check_in => "07/08/2018",
        :check_out => "07/09/2018",
      ),
      Booking.create!(
      :check_in => "07/08/2018",
      :check_out => "07/09/2018",
      )
    ])
  end

  it "renders a list of bookings" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 4
  end
end
