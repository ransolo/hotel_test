class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update, :destroy]
  respond_to :json, :html, only: :index

  # GET /rooms
  # GET /rooms.json
  def index
    set_hotel
    @room = @hotel.rooms.new
    @rooms = Room.find params[:hotel_id]
    respond_with @rooms
  end

  # GET /rooms/1
  # GET /rooms/1.json
  def show
    @booking = @room.bookings.new
    @rooms = Room.all
    @bookings = Booking.where(room_id: @room.id)
  end

  # GET /rooms/new
  def new
    set_hotel
    @room = @hotel.rooms.new
    
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  # POST /rooms.json
  def create
    set_hotel
    @room = @hotel.rooms.new(room_params)
    respond_to do |format|
      if @room.save
        format.html { redirect_to hotel_room_path(@hotel, @room), notice: 'Room was successfully created.' }
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    set_hotel
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to hotel_room_path(@hotel, @room), notice: 'Room was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    set_hotel
    @room.destroy
    respond_to do |format|
      format.html { redirect_to hotel_path(@hotel), notice: 'Room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_room
    @room = Room.find(params[:id])
  end

  def set_hotel
    @hotel = Hotel.find(params[:hotel_id])
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def room_params
    params.require(:room).permit(:number, :category, :description, :room_type_id)
  end
end
