class RoomTypesController < InheritedResources::Base

  private

    def room_type_params
      params.require(:room_type).permit(:name, :price)
    end
end

