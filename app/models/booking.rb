class Booking < ApplicationRecord
  belongs_to :user
  belongs_to :room
  validate :check_in_is_valid_datetime
  validate :check_out_is_valid_datetime

  def check_in_is_valid_datetime
    errors.add(:check_in, 'must be a valid datetime') if ((DateTime.parse(check_in.to_s) rescue ArgumentError) == ArgumentError)
  end

  def check_out_is_valid_datetime
    errors.add(:check_out, 'must be a valid datetime') if ((DateTime.parse(check_out.to_s) rescue ArgumentError) == ArgumentError)
  end
end
