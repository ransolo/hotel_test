class Room < ApplicationRecord
  has_many :bookings
  belongs_to :hotel, counter_cache: :room_count
  belongs_to :room_type
  validates_presence_of :number
  validates_numericality_of :number
end
