json.array! @rooms, partial: 'rooms/room', as: :room
json.array!(@rooms) do |room|
  json.extract! room, :id, :number, :hotel
  json.url rooms_url(room, format: :json)
end
