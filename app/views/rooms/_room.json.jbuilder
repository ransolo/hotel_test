json.extract! room, :id, :number, :category, :description, :created_at, :updated_at
json.url room_url(room, format: :json)
