
### What is this repository for? ###

Takl code test

# To do #

##### Singular booking form #####

The way it is, you select a hotel then a room, then make your booking on the room
[![https://gyazo.com/5ea984331827db936fc560798b315a1e](https://i.gyazo.com/5ea984331827db936fc560798b315a1e.gif)](https://gyazo.com/5ea984331827db936fc560798b315a1e)

##### Calendar view #####

Implement full_calendar.js to display a users bookings

##### Booking Index #####
# fixed#
Verify that a user only sees their bookings. 
If there are issues implement cancan correctly, at one point I had it working, but
it was not in booking and was removed.

[![https://gyazo.com/f66e967ca2cfba2131cb5d8e9b6fc352](https://i.gyazo.com/f66e967ca2cfba2131cb5d8e9b6fc352.png)](https://gyazo.com/f66e967ca2cfba2131cb5d8e9b6fc352)

##### implement datetime picker #####

https://eonasdan.github.io/bootstrap-datetimepicker/

##### Style #####

All is very bootstrapped styling with no love
This does not represent my abilities in the front end.

##### Stripe #####

Implement stripe checkout

# rspec #

This is my first project using rspec.  I have used minitest in the past. 
I spent some time working on nested resources and associations.  

# Versions #

I used rails 5 and ruby 2.3.3 because in the beginning I was having trouble with rails g generating 
mismatched code for the rails 4 app, actually I think it was rails new.
I have worked plenty in rails 4 and am capable in it I just couldn't afford to put more
time to the issue I was having.
 
# in retrospect #
I would write myself better specs in the beginning.  Creating issues that clearly define the project.
I got carried away and cowboy coded it going far too long between commits.
